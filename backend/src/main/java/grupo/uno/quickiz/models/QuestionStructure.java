package grupo.uno.quickiz.models;

import lombok.Getter;
import lombok.Setter;
import lombok.AccessLevel;
import javax.persistence.*;
import java.io.Serializable;
import grupo.uno.quickiz.models.Pool;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Setter
@Getter
@Entity
@Table(name = "question_structures")
@SuppressWarnings("serial")
public class QuestionStructure implements Serializable {

    @Id
    @GeneratedValue(
        strategy = GenerationType.AUTO
    )
    @Column(
        name = "id",
        unique = true,
        nullable = false
    )
    private Integer id;

    @Column(name = "score")
    private Integer score;

    // @JsonManagedReference
    // @ManyToOne
    // @JoinColumn(name="quiz_structure_id")
    // @Getter(AccessLevel.NONE)
    // private QuizStructure quizStructure;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name="pool_id")
    @Getter(AccessLevel.NONE)
    private Pool pool;

    public Pool getPool() {
        return this.pool;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public void setPool(Pool pool) {
		this.pool = pool;
	}

    // public QuizStructuer getQuizStructure() {
        // return this.quizStructure;
    // }
    
    

}

