package grupo.uno.quickiz.models;

import lombok.Getter;
import lombok.Setter;
import lombok.AccessLevel;
import javax.persistence.*;
import java.io.Serializable;
import grupo.uno.quickiz.models.Role;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Setter
@Getter
@Entity
@Table(name = "users")
@SuppressWarnings("serial")
public class User implements Serializable {

    @Id
    @GeneratedValue(
        strategy = GenerationType.AUTO
    )
    @Column(
        name = "id",
        unique = true,
        nullable = false
    )
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "email")
    private String email;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name="role_id")
    @Getter(AccessLevel.NONE)
    private Role role;

    public Role getRole() {
        return this.role;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public void setRole(Role role) {
		this.role = role;
	}
    
    

}
