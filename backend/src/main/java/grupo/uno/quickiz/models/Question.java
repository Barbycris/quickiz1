package grupo.uno.quickiz.models;

import lombok.Getter;
import lombok.Setter;
import lombok.AccessLevel;
import javax.persistence.*;
import java.io.Serializable;
import grupo.uno.quickiz.models.Pool;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Setter
@Getter
@Entity
@Table(name = "questions")
@SuppressWarnings("serial")
public class Question implements Serializable {

    @Id
    @GeneratedValue(
        strategy = GenerationType.AUTO
    )
    @Column(
        name = "id",
        unique = true,
        nullable = false
    )
    private Integer id;

    @Column(name = "title")
    private String title;

    @Column(name = "code")
    private String code;

    @Column(name = "variables")
    private String variables; //Json

    @Column(name = "user_id")
    private Integer user_id;

    @JsonManagedReference
    @ManyToOne
    @JoinColumn(name="pool_id")
    @Getter(AccessLevel.NONE)
    private Pool pool;

    public Pool getPool() {
        return this.pool;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getVariables() {
		return variables;
	}

	public void setVariables(String variables) {
		this.variables = variables;
	}

	public Integer getUser_id() {
		return user_id;
	}

	public void setUser_id(Integer user_id) {
		this.user_id = user_id;
	}

	public void setPool(Pool pool) {
		this.pool = pool;
	}
    
    

}

