package grupo.uno.quickiz.models;

import java.util.*;
import lombok.Getter;
import lombok.Setter;
import lombok.AccessLevel;
import javax.persistence.*;
import java.io.Serializable;
import grupo.uno.quickiz.models.Pool;
import grupo.uno.quickiz.models.QuestionStructure;
import com.fasterxml.jackson.annotation.JsonBackReference;

@Setter
@Getter
@Entity
@Table(name = "quiz_structures")
@SuppressWarnings("serial")
public class QuizStructure implements Serializable {

    @Id
    @GeneratedValue(
        strategy = GenerationType.AUTO
    )
    @Column(
        name = "id",
        unique = true,
        nullable = false
    )
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "duration")
    private Integer duration; // minutes

    @Column(name = "salck_time")
    private Integer slackTime; // minutes

    @JsonBackReference
    @OneToMany
    @Getter(AccessLevel.NONE)
    private List<QuestionStructure> questionStructures = new ArrayList<>();

    public List<QuestionStructure> getQuestionStructures() {
        return this.questionStructures;
    }

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Integer getDuration() {
		return duration;
	}

	public void setDuration(Integer duration) {
		this.duration = duration;
	}

	public Integer getSlackTime() {
		return slackTime;
	}

	public void setSlackTime(Integer slackTime) {
		this.slackTime = slackTime;
	}

	public void setQuestionStructures(List<QuestionStructure> questionStructures) {
		this.questionStructures = questionStructures;
	}
    
    

}

