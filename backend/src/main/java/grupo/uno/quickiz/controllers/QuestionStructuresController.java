package grupo.uno.quickiz.controllers;

import java.util.List;
import grupo.uno.quickiz.models.Pool;
import org.springframework.http.HttpStatus;
import grupo.uno.quickiz.models.QuestionStructure;
import grupo.uno.quickiz.repositories.PoolRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;
import grupo.uno.quickiz.repositories.QuestionStructureRepository;


@RestController
@RequestMapping("questionstructures")
// @CrossOrigin(origins = "http://159.65.253.247:80")
public class QuestionStructuresController {

    @Autowired
    private QuestionStructureRepository questionStructureRepository;
    @Autowired
    private PoolRepository poolRepository;

    @RequestMapping(value = "", method = RequestMethod.GET)
    public List<QuestionStructure> getQuestionStructures()
    {
        return questionStructureRepository.findAll();
    }

    @RequestMapping(value = "{id}", method = RequestMethod.GET, produces = "application/json")
    public QuestionStructure getQuestionStructureById(@PathVariable Integer id) {
        return questionStructureRepository.findById(id).get();
    }

    @RequestMapping(value="", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public QuestionStructure createQuestionStructure(@RequestBody QuestionStructure questionStructure)
    {
        return questionStructureRepository.save(questionStructure);
    }

    @RequestMapping(value="{id}", method = RequestMethod.PUT)
    public QuestionStructure updateQuestionStructure(@RequestBody QuestionStructure questionStructure, @PathVariable Integer id) {

        QuestionStructure q = questionStructureRepository.findById(id).get();
        // verify if exist

        q.setScore(questionStructure.getScore());

        questionStructureRepository.save(q);
        return q;
    }

    @RequestMapping(value = "{id}", method = RequestMethod.DELETE)
    public void deleteQuestionStructure(@PathVariable Integer id) {
        // verify if exist
        questionStructureRepository.deleteById(id);
    }

    @RequestMapping(value = "pool/{id}", method = RequestMethod.GET)
    public List<QuestionStructure> poolQuestionStructures(@PathVariable Integer id) {
        // verify if exist
        Pool pool = poolRepository.findById(id).get();
        return pool.getQuestionStructures();
    }
}

