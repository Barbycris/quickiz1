package grupo.uno.quickiz.controllers;

import java.util.List;
import grupo.uno.quickiz.models.Pool;
import org.springframework.http.HttpStatus;
import grupo.uno.quickiz.repositories.PoolRepository;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.beans.factory.annotation.Autowired;


@RestController
// @CrossOrigin(origins = "http://159.65.253.247:80")
public class PoolsController {

    @Autowired
    private PoolRepository poolRepository;

    @RequestMapping(value = "/pools", method = RequestMethod.GET)
    public List<Pool> getPools()
    {
        return poolRepository.findAll();
        //load questions
    }

    @RequestMapping(value = "/pools/{id}", method = RequestMethod.GET, produces = "application/json")
    public Pool getPoolById(@PathVariable Integer id) {
        return poolRepository.findById(id).get();
    }

    @RequestMapping(value="/pools", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.CREATED)
    @ResponseBody
    public Pool createPool(@RequestBody Pool pool)
    {
        return poolRepository.save(pool);
    }

    @RequestMapping(value="/pools/{id}", method = RequestMethod.PUT)
    public Pool updatePool(@RequestBody Pool pool, @PathVariable Integer id) {

        Pool p = poolRepository.findById(id).get();
        // verify if exist

        p.setName(pool.getName());

        poolRepository.save(p);
        return p;
    }

    @RequestMapping(value = "/pools/{id}", method = RequestMethod.DELETE)
    public void deletePool(@PathVariable Integer id) {
        // verify if exist
        poolRepository.deleteById(id);
    }
}

