package grupo.uno.quickiz.repositories;

import java.util.List;
import grupo.uno.quickiz.models.Question;
import org.springframework.stereotype.Repository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Transactional;


@Repository
@Transactional
public interface QuestionRepository extends CrudRepository<Question, Integer> {

    List<Question> findAll();
    Question findByTitle(String title);
    void deleteByTitle(String title);

}

