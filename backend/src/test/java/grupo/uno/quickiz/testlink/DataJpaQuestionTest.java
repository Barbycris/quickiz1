package grupo.uno.quickiz.testlink;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import grupo.uno.quickiz.models.Question;
import grupo.uno.quickiz.repositories.QuestionRepository;


@RunWith(SpringRunner.class)
@DataJpaTest 
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class DataJpaQuestionTest {
	
	@Autowired
	private TestEntityManager em;
	
	@Autowired
	private QuestionRepository questionRepository;
	
	@Test
	public void QuestionSaveTest() {
		Question question = getQuestion("Enteros, reales y operaciones aritméticas.");
		Question questionToDB = em.persist(question);
		em.flush();
		
		Question questionFromDB = questionRepository.findById(questionToDB.getId()).orElse(null);
		
		assertThat(questionFromDB.getTitle()).isEqualTo(question.getTitle());
	}
	
	@Test
	public void getQuestionByIdTest() {
		Question question = getQuestion("Booleanos, operadores lógicos y cadenas.");
		
		//Guardar question en la base de datos.
		Question questionToDB = em.persist(question);
		em.flush();
		
		//Obtenerlo de la base de datos.
		Question questionFromDB = questionRepository.findById(questionToDB.getId()).orElse(null);
		
		assertThat(questionToDB).isEqualTo(questionFromDB);
	}
	
	@Test
	public void getAllQuestionTest() {
		Question question = getQuestion("Listas");
		Question question2 = getQuestion("Tuplas");
		Question question3 = getQuestion("Diccionarios");
		
		//Guardar Question y Question2 en la base de datos.
		em.persist(question);
		em.persist(question2);
		em.persist(question3);
		
		
		//Obtener todos los Question desde la base de datos.
		List<Question> allQuestionFromDB = questionRepository.findAll();
		List<Question> questionList = new ArrayList<>();
		
		for(Question questioni : allQuestionFromDB) {
			questionList.add(questioni);
		}
		
		assertThat(questionList.size()).isEqualTo(allQuestionFromDB.size());
	}
	
	
	@Test
	public void deleteQuestionByIdTest() {
		Question question = getQuestion("Operadores relacionales.");
		Question question2 = getQuestion("Sentencias condicionales.");
		
		//Guardar question y question2 en la base de datos.
		Question questionToDD = em.persist(question);
		em.persist(question2);
		em.flush();
		
		//Obtener todos los question desde la base de datos.
		List<Question> allquestionFromDB = questionRepository.findAll();
		
		//ELiminar questionToDb.
		em.remove(questionToDD);
		
		//Obtener todos los question desde la base de datos tras eliminar questionToDB.
		List<Question> allquestionFromDB2 = questionRepository.findAll();
		
		List<Question> questionList = new ArrayList<>();
		
		for(Question questioni : allquestionFromDB2) {
			questionList.add(questioni);
		}
		
		assertThat(questionList.size()).isEqualTo((allquestionFromDB.size()-1));
	}
	
	
	@Test
	public void updatequestionTest() {
		Question question = getQuestion("Sentencias condicionales.");
		
		//Guardar question y question2 en la base de datos.
		em.persist(question);
		em.flush();
		
		//Obtener question desde la base de datos.
		Question questionFromDB = questionRepository.findById(question.getId()).orElse(null);
		
		//Actualizar tema de questionFromDb.
		questionFromDB.setTitle("Bucles.");
		em.persist(questionFromDB);
		
		
		assertThat(questionFromDB.getTitle()).isEqualTo("Bucles.");
	}

	public Question getQuestion(String tema) {
		Question question = new Question();
		question.setTitle(tema);
		return question;
	}
}
