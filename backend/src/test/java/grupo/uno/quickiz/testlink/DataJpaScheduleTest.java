package grupo.uno.quickiz.testlink;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import grupo.uno.quickiz.models.Schedule;
import grupo.uno.quickiz.repositories.ScheduleRepository;

@RunWith(SpringRunner.class)
@DataJpaTest 
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class DataJpaScheduleTest {
	
	@Autowired
	private TestEntityManager em;
	
	@Autowired
	private ScheduleRepository scheduleRepository;
	
	@Test
	public void scheduleSaveTest() {
		Schedule schedule = getSchedule("Prueba1" );
		Schedule scheduleToDB = em.persist(schedule);
		em.flush();
		
		Schedule scheduleFromDB = scheduleRepository.findById(scheduleToDB.getId()).orElse(null);
		
		assertThat(scheduleFromDB.getCode()).isEqualTo(schedule.getCode());
	}
	
	@Test
	public void geScheduletByIdTest() {
		Schedule schedule = getSchedule("Prueba2" );
		
		//Guardar schedule en la base de datos.
		Schedule scheduleToDB = em.persist(schedule);
		em.flush();
		
		//Obtenerlo de la base de datos.
		Schedule scheduleFromDB = scheduleRepository.findById(scheduleToDB.getId()).orElse(null);
		
		assertThat(scheduleToDB).isEqualTo(scheduleFromDB);
	}
	
	@Test
	public void getAllScheduleTest() {
		Schedule schedule = getSchedule("Prueba3" );
		Schedule schedule2 = getSchedule("Prueba4" );
		Schedule schedule3 = getSchedule("Prueba5" );
		
		//Guardar schedule, schedule2 y schedule3 en la base de datos.
		em.persist(schedule);
		em.persist(schedule2);
		em.persist(schedule3);
		
		
		//Obtener todos los schedule desde la base de datos.
		List<Schedule> allScheduleFromDB = scheduleRepository.findAll();
		List<Schedule> scheduleList = new ArrayList<>();
		
		for(Schedule schedulei : allScheduleFromDB) {
			scheduleList.add(schedulei);
		}
		
		assertThat(scheduleList.size()).isEqualTo(allScheduleFromDB.size());
	}
	
	@Test
	public void deleteScheduleByIdTest() {
		Schedule schedule = getSchedule("Prueba6" );
		Schedule schedule2 = getSchedule("Prueba7" );
		
		//Guardar schedule y schedule2 en la base de datos.
		Schedule scheduleToDD = em.persist(schedule);
		em.persist(schedule2);
		em.flush();
		
		//Obtener todos los schedule desde la base de datos.
		List<Schedule> allScheduleFromDB = scheduleRepository.findAll();
		
		//ELiminar scheduleToDb.
		em.remove(scheduleToDD);
		
		//Obtener todos los schedule desde la base de datos tras eliminar scheduleToDB.
		List<Schedule> allScheduleFromDB2 = scheduleRepository.findAll();
		
		List<Schedule> scheduleList = new ArrayList<>();
		
		for(Schedule schedulei : allScheduleFromDB2) {
			scheduleList.add(schedulei);
		}
		
		assertThat(scheduleList.size()).isEqualTo((allScheduleFromDB.size()-1));
	}
	
	@Test
	public void updateScheduleTest() {
		Schedule schedule = getSchedule("Prueba8" );
		
		//Guardar schedule  en la base de datos.
		em.persist(schedule);
		em.flush();
		
		//Obtener schedule desde la base de datos.
		Schedule scheduleFromDB = scheduleRepository.findById(schedule.getId()).orElse(null);
		
		//Actualizar code de scheduleFromDb.
		scheduleFromDB.setCode("Prueba9");
		em.persist(scheduleFromDB);
		
		
		assertThat(scheduleFromDB.getCode()).isEqualTo("Prueba9");
	}
	
	public Schedule getSchedule(String code) {
		Schedule schedule = new Schedule();
		schedule.setCode(code);
		return schedule;
		
	}

}
