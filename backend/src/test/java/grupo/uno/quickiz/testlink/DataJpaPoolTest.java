package grupo.uno.quickiz.testlink;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;


import grupo.uno.quickiz.models.Pool;
import grupo.uno.quickiz.repositories.PoolRepository;

@RunWith(SpringRunner.class)
@DataJpaTest 
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class DataJpaPoolTest {
	
	@Autowired
	private TestEntityManager em;
	
	@Autowired
	private PoolRepository poolRepository;
	
	@Test
	public void poolSaveTest() {
		Pool pool = getPool("Enteros, reales y operaciones aritméticas.");
		Pool poolToDB = em.persist(pool);
		em.flush();
		
		Pool poolFromDB = poolRepository.findById(poolToDB.getId()).orElse(null);
		
		assertThat(poolFromDB.getName()).isEqualTo(pool.getName());
	}
	
	@Test
	public void getPoolByIdTest() {
		Pool pool = getPool("Booleanos, operadores lógicos y cadenas.");
		
		//Guardar pool en la base de datos.
		Pool poolToDB = em.persist(pool);
		em.flush();
		
		//Obtenerlo de la base de datos.
		Pool poolFromDB = poolRepository.findById(poolToDB.getId()).orElse(null);
		
		assertThat(poolToDB).isEqualTo(poolFromDB);
	}
	
	
	@Test
	public void getAllPoolTest() {
		Pool pool = getPool("Listas");
		Pool pool2 = getPool("Tuplas");
		Pool pool3 = getPool("Diccionarios");
		
		//Guardar pool y pool2 en la base de datos.
		em.persist(pool);
		em.persist(pool2);
		em.persist(pool3);
		
		
		//Obtener todos los pool desde la base de datos.
		List<Pool> allPoolFromDB = poolRepository.findAll();
		List<Pool> poolList = new ArrayList<>();
		
		for(Pool pooli : allPoolFromDB) {
			poolList.add(pooli);
		}
		
		assertThat(poolList.size()).isEqualTo(allPoolFromDB.size());
	}
	
	@Test
	public void deletePoolByIdTest() {
		Pool pool = getPool("Operadores relacionales.");
		Pool pool2 = getPool("Sentencias condicionales.");
		
		//Guardar pool y pool2 en la base de datos.
		Pool poolToDD = em.persist(pool);
		em.persist(pool2);
		em.flush();
		
		//Obtener todos los pool desde la base de datos.
		List<Pool> allPoolFromDB = poolRepository.findAll();
		
		//ELiminar poolToDb.
		em.remove(poolToDD);
		
		//Obtener todos los pool desde la base de datos tras eliminar poolToDB.
		List<Pool> allPoolFromDB2 = poolRepository.findAll();
		
		List<Pool> poolList = new ArrayList<>();
		
		for(Pool pooli : allPoolFromDB2) {
			poolList.add(pooli);
		}
		
		assertThat(poolList.size()).isEqualTo((allPoolFromDB.size()-1));
	}
	
	@Test
	public void updatePoolTest() {
		Pool pool = getPool("Sentencias condicionales.");
		
		//Guardar pool y pool2 en la base de datos.
		em.persist(pool);
		em.flush();
		
		//Obtener pool desde la base de datos.
		Pool poolFromDB = poolRepository.findById(pool.getId()).orElse(null);
		
		//Actualizar tema de poolFromDb.
		poolFromDB.setName("Bucles.");
		em.persist(poolFromDB);
		
		
		assertThat(poolFromDB.getName()).isEqualTo("Bucles.");
	}
	
	
	

	
	public Pool getPool(String tema) {
		Pool pool = new Pool();
		pool.setName(tema);
		return pool;
		
	}
	
}
