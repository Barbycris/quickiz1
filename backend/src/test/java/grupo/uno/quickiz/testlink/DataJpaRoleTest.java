package grupo.uno.quickiz.testlink;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

import grupo.uno.quickiz.models.Role;
import grupo.uno.quickiz.repositories.RoleRepository;

@RunWith(SpringRunner.class)
@DataJpaTest 
@AutoConfigureTestDatabase(replace = Replace.NONE)
public class DataJpaRoleTest {
	
	@Autowired
	private TestEntityManager em;
	
	@Autowired
	private RoleRepository roleRepository;
	
	@Test
	public void roleSaveTest() {
		Role role = getRole("Prueba1" );
		Role roleToDB = em.persist(role);
		em.flush();
		
		Role roleFromDB = roleRepository.findById(roleToDB.getId()).orElse(null);
		
		assertThat(roleFromDB.getName()).isEqualTo(role.getName());
	}
	
	@Test
	public void geRoletByIdTest() {
		Role role = getRole("Prueba2" );
		
		//Guardar role en la base de datos.
		Role roleToDB = em.persist(role);
		em.flush();
		
		//Obtenerlo de la base de datos.
		Role roleFromDB = roleRepository.findById(roleToDB.getId()).orElse(null);
		
		assertThat(roleToDB).isEqualTo(roleFromDB);
	}
	
	@Test
	public void getAllRoleTest() {
		Role role = getRole("Prueba3" );
		Role role2 = getRole("Prueba4" );
		Role role3 = getRole("Prueba5" );
		
		//Guardar role, role2 y role3 en la base de datos.
		em.persist(role);
		em.persist(role2);
		em.persist(role3);
		
		
		//Obtener todos los role desde la base de datos.
		List<Role> allRoleFromDB = roleRepository.findAll();
		List<Role> roleList = new ArrayList<>();
		
		for(Role rolei : allRoleFromDB) {
			roleList.add(rolei);
		}
		
		assertThat(roleList.size()).isEqualTo(allRoleFromDB.size());
	}
	
	@Test
	public void deleteRoleByIdTest() {
		Role role = getRole("Prueba6" );
		Role role2 = getRole("Prueba7" );
		
		//Guardar role y role2 en la base de datos.
		Role roleToDD = em.persist(role);
		em.persist(role2);
		em.flush();
		
		//Obtener todos los role desde la base de datos.
		List<Role> allRoleFromDB = roleRepository.findAll();
		
		//ELiminar roleToDb.
		em.remove(roleToDD);
		
		//Obtener todos los role desde la base de datos tras eliminar roleToDB.
		List<Role> allRoleFromDB2 = roleRepository.findAll();
		
		List<Role> roleList = new ArrayList<>();
		
		for(Role rolei : allRoleFromDB2) {
			roleList.add(rolei);
		}
		
		assertThat(roleList.size()).isEqualTo((allRoleFromDB.size()-1));
	}
	
	@Test
	public void updateRoleTest() {
		Role role = getRole("Prueba8" );
		
		//Guardar role  en la base de datos.
		em.persist(role);
		em.flush();
		
		//Obtener role desde la base de datos.
		Role roleFromDB = roleRepository.findById(role.getId()).orElse(null);
		
		//Actualizar name de roleFromDb.
		roleFromDB.setName("Prueba9");
		em.persist(roleFromDB);
		
		
		assertThat(roleFromDB.getName()).isEqualTo("Prueba9");
	}
	
	public Role getRole(String name) {
		Role role = new Role();
		role.setName(name);
		return role;
		
	}

}
