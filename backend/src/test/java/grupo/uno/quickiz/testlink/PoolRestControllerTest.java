package grupo.uno.quickiz.testlink;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.RequestBuilder;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import grupo.uno.quickiz.controllers.PoolsController;
import grupo.uno.quickiz.models.Pool;
import grupo.uno.quickiz.repositories.PoolRepository;

@RunWith(SpringRunner.class)
@WebMvcTest(value=PoolsController.class, secure = false)
public class PoolRestControllerTest {

	@Autowired
	private MockMvc mockMvc;
	
	@MockBean
	private PoolRepository poolRepository;
	
	@Test
	public void createPoolTest() throws Exception {
		Pool mockPool = new Pool();
		mockPool.setId(1);
		mockPool.setName("Enteros, reales y operaciones aritméticas.");
		
		//COnvertir el objeto a Json
		String inputInJson = this.mapToJson(mockPool);
		
		//Url del metodo create
		String URI = "/pools";
		
		//Cuando se cree cualquier tipo de Pool retorne "mockPool".
		Mockito.when(poolRepository.save(Mockito.any(Pool.class))).thenReturn(mockPool);
		
		//Constructor del objeto JSON.
		RequestBuilder requestBuilder = MockMvcRequestBuilders
				.post(URI)
				.accept(MediaType.APPLICATION_JSON).content(inputInJson)
				.contentType(MediaType.APPLICATION_JSON);
		
		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		MockHttpServletResponse response = result.getResponse();
		
		String outputInJson = response.getContentAsString();
		
		assertThat(outputInJson).isEqualTo(inputInJson);
		assertEquals(HttpStatus.OK.value(), response.getStatus());
		
	}
	
	@Test
	public void getPoolByIdTest() throws Exception {
		Pool mockPool = new Pool();
		mockPool.setId(1);
		mockPool.setName("Booleanos, operadores lógicos y cadenas.");
		
		Mockito.when(poolRepository.findById(Mockito.anyInt()).orElse(null)).thenReturn(mockPool);
		
		//Url del metodo GET by id.
		String URI = "/pools/1";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				URI).accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();
		
		String expectedJson = this.mapToJson(mockPool);
		String outputInJson = result.getResponse().getContentAsString();
		
		assertThat(outputInJson).isEqualTo(expectedJson);
		
	}
	
	@Test
	public void getAllPoolTest() throws Exception {
		Pool mockPool = new Pool();
		mockPool.setId(1);
		mockPool.setName("Listas.");
		
		Pool mockPool2 = new Pool();
		mockPool.setName("Tuplas.");
		
		List<Pool> poolList = new ArrayList<>();
		poolList.add(mockPool);
		poolList.add(mockPool2);
		
		Mockito.when(poolRepository.findAll()).thenReturn(poolList);
		
		//Url del metodo GET by id.
		String URI = "/pools";
		
		RequestBuilder requestBuilder = MockMvcRequestBuilders.get(
				URI).accept(
				MediaType.APPLICATION_JSON);

		MvcResult result = mockMvc.perform(requestBuilder).andReturn();

		String expectedJson = this.mapToJson(poolList);
		String outputInJson = result.getResponse().getContentAsString();
		assertThat(outputInJson).isEqualTo(expectedJson);
		
	}

	/**
	 * Maps an Object into a JSON String. Uses a Jackson ObjectMapper.
	 */
	private String mapToJson(Object object) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(object);
	}
	
	
	
	
	
	
	
}
