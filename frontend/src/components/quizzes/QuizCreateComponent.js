import React, { Component }from 'react';

import { Link } from 'react-router-dom';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class QuizCreateComponent extends Component {
  state = {
    quiz: {
      name: '',
      questions: []
    }
  };

  render() {
    return (
      <div>
        <Typography variant="h4" gutterBottom>Crear Quiz</Typography>
        <form>
          <TextField
                    id="filled-full-width"
                    label="Nombre"
                    placeholder="Ingrese nombre del quiz"
                    fullWidth
                    margin="normal"
                    variant="filled"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />
            

          <Button component={Link} to="/pools">
            Cancelar
          </Button>
          <Button variant="contained" color="primary" type="submit">
            Crear
          </Button>
        </form>
      </div>
    );
  }
}

export default QuizCreateComponent;