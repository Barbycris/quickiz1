import React, { Component }from 'react';

import { Link } from 'react-router-dom';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

class QuizShowComponent extends Component {
  state = {
    quiz: {
      id: 1,
      name: 'Quiz #1',
      questions: [
        { topic_id: 'Expresiones Matematicas', points: 3 },
        { topic_id: 'Expresiones Matematicas', points: 3 },
      ]
    }
  };

  render() {
    return (
      <div>
        <Typography variant="h4" gutterBottom>Pozo: { this.state.quiz.name }</Typography>
        <Paper>
        
        {
          this.state.quiz.questions.map(question => {
            return (
              <Typography key={ question.id }>
                { question.topic_id + " (" + question.points + ")" }
              </Typography>
            );
          })
        }
        </Paper>

        <Button component={Link} to="/quizzes">
          Volver
        </Button>
      </div>
    );
  }
}

export default QuizShowComponent;