import React, { Component }from 'react';

import { Link } from 'react-router-dom';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';
import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';


class QuestionIndexComponent extends Component {
  state = {
    questions: [
      { id: 1, topic: 'Expresiones Matematicas', enunciado: 'Pregunta #1' },
      { id: 2, topic: 'Estructura de scripting', enunciado: 'Pregunta #2' },
      { id: 3, topic: 'Expresiones booleanas', enunciado: 'Pregunta #3' },
      { id: 4, topic: 'Decisiones', enunciado: 'Pregunta #4' },
    ],
  };

  render() {
    return (
      <div>
        <Typography variant="h4" gutterBottom>Listado de preguntas</Typography>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Pozo</TableCell>
                <TableCell>Enunciado</TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.questions.map(question => {
                return (
                  <TableRow key={question.id}>
                    <TableCell component="th" scope="question">
                      { question.topic }
                    </TableCell>
                    <TableCell>
                      { question.enunciado }
                    </TableCell>
                    <TableCell>
                      <Button color="default" component={Link} to={"/questions/"+question.id}>
                        Ver
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button color="primary" component={Link} to={"/questions/"+question.id+"/edit"}>
                        Editar
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button color="secondary" component={Link} to={"/questions/"+question.id+"/delete"}>
                        Borrar
                      </Button>
                    </TableCell>
                  </TableRow>
                );
              })}
            </TableBody>
          </Table>
        </Paper>

        <Button variant="contained" color="primary" component={Link} to="/questions/create">
          Crear Nueva Pregunta
        </Button>
      </div>
    );
  }
}

export default QuestionIndexComponent;