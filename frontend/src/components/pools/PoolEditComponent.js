import React, { Component }from 'react';

import { Link } from 'react-router-dom';
import axios from 'axios';

import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

const URL_API = 'http://localhost:3004';

class PoolEditComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pool: {
        id: this.props.match.params.id,
        name: '',
        questions: []
      }
    };

    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  componentDidMount() {
    this.getPool()
  }

  getPool() {
    axios.get(URL_API + '/pools/' + this.state.pool.id)
      .then(result => this.setState({
        pool: result.data,
      }))
      .catch(error => this.setState({
        pool: []
      }));
  }

  validateSubmit(event) {
    // const form = event.target;
    const pool = this.state.pool;
    var outcome = true;
    // const properties = [
    //   'name'
    // ];
    
    try {
      if (pool.name.length === 0 || pool.name.length > 256) {
        outcome = false;
        console.error("pool.name invalid");
        // form.elements['name'].classList += " invalid";
        // window.M.toast({html: 'Largo del nombre no puede ser mayor a 256 caracteres'});
      }
    } catch (err) {
      console.log(err);
      // window.M.toast({html: 'Whoops, ha ocurrido un error'});
    }

    event.preventDefault();
    event.stopPropagation();
    return outcome;
  }

  handleSubmit(event) {
    if ( ! this.validateSubmit(event)) {
      event.preventDefault();
      return;
    }

    axios.put(URL_API + '/pools/' + this.state.pool.id, this.state.pool)
      .then(result => {
        this.setState({
          pool: result.data
        });

        this.props.history.push('/pools');
        console.log('done');
        // window.M.toast({html: 'Producto Creado'});
      })
      .catch(error => {
        console.log(error);
        // window.M.toast({html: 'Error al agregar el poolo'});
      });

    event.preventDefault();
  }

  handleChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    this.setState({
      pool: {
        ...this.state.pool,
        [name]: value
      }
    });
  }

  render() {
    return (
      <div>
        <Typography variant="h4" gutterBottom>Editar Pozo</Typography>
        <form onSubmit={this.handleSubmit}>
          <TextField
                    id="filled-full-width"
                    label="Nombre"
                    type="text"
                    name="name"
                    value={this.state.pool.name}
                    onChange={this.handleChange}
                    placeholder="Ingrese tematica del pozo"
                    fullWidth
                    margin="normal"
                    variant="filled"
                    InputLabelProps={{
                      shrink: true,
                    }}
                  />

          <Button component={Link} to="/pools">
            Cancelar
          </Button>
          <Button variant="contained" color="primary" type="submit">
            Guardar
          </Button>
        </form>
      </div>
    );
  }
}

export default PoolEditComponent;