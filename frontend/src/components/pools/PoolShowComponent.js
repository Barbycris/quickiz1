import React, { Component }from 'react';

import { Link } from 'react-router-dom';
import axios from 'axios';

import Paper from '@material-ui/core/Paper';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';

import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import TableHead from '@material-ui/core/TableHead';
import TableRow from '@material-ui/core/TableRow';

const URL_API = 'http://localhost:3004';

class PoolShowComponent extends Component {
  constructor(props) {
    super(props);

    this.state = {
      pool: {
        id: this.props.match.params.id,
        name: '',
        questions: []
      }
    };

    // this.delete = this.delete.bind(this);
  }

  componentDidMount() {
    this.getPool()
  }

  getPool() {
    axios.get(URL_API + '/pools/' + this.state.pool.id)
      .then(result => this.setState({
        pool: result.data,
      }))
      .catch(error => this.setState({
        pool: []
      }));
  }

  render() {
    return (
      <div>
        <Typography variant="h4" gutterBottom>Pozo: { this.state.pool.name }</Typography>
        <Paper>
          <Table>
            <TableHead>
              <TableRow>
                <TableCell>Enunciado de la pregunta</TableCell>
                <TableCell>Código</TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
                <TableCell></TableCell>
              </TableRow>
            </TableHead>
            <TableBody>
              {this.state.pool.questions.map(question => {
                return (
                  <TableRow key={question.id}>
                    <TableCell component="th" scope="pool">
                      { question.enunciado }
                    </TableCell>
                    <TableCell>
                      <code>
                        { question.code }
                      </code>
                    </TableCell>
                    <TableCell>
                      <Button color="default" component={Link} to={"/questions/"+question.id}>
                        Ver
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button color="primary" component={Link} to={"/questions/"+question.id+"/edit"}>
                        Editar
                      </Button>
                    </TableCell>
                    <TableCell>
                      <Button color="secondary" component={Link} to={"/questions/"+question.id+"/delete"}>
                        Remover
                      </Button>
                    </TableCell>
                  </TableRow>
                );
              })}
              </TableBody>
          </Table>
        </Paper>

        <Button color="default" component={Link} to="/pools/">
          Volver
        </Button>

        <Button variant="contained" color="primary" component={Link} to="/questions/create">
          Agregar Pregunta
        </Button>
      </div>
    );
  }
}

export default PoolShowComponent;