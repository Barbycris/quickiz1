import React, { Component }from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import List from '@material-ui/core/List';
import CssBaseline from '@material-ui/core/CssBaseline';
import Typography from '@material-ui/core/Typography';
import Divider from '@material-ui/core/Divider';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import ChevronLeftIcon from '@material-ui/icons/ChevronLeft';
import ChevronRightIcon from '@material-ui/icons/ChevronRight';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import StyleIcon from '@material-ui/icons/Style';
import HelpIcon from '@material-ui/icons/Help';
import AssignmentIcon from '@material-ui/icons/Assignment';
import EditIcon from '@material-ui/icons/Edit';

import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import HomeComponent from './components/HomeComponent';

import PoolIndexComponent from './components/pools/PoolIndexComponent';
import PoolCreateComponent from './components/pools/PoolCreateComponent';
import PoolShowComponent from './components/pools/PoolShowComponent';
import PoolEditComponent from './components/pools/PoolEditComponent';

import QuestionIndexComponent from './components/questions/QuestionIndexComponent';
import QuestionCreateComponent from './components/questions/QuestionCreateComponent';
import QuestionShowComponent from './components/questions/QuestionShowComponent';
import QuestionEditComponent from './components/questions/QuestionEditComponent';

import QuizIndexComponent from './components/quizzes/QuizIndexComponent';
import QuizCreateComponent from './components/quizzes/QuizCreateComponent';
import QuizShowComponent from './components/quizzes/QuizShowComponent';
import QuizEditComponent from './components/quizzes/QuizEditComponent';
import MyQuizzesComponent from './components/quizzes/MyQuizzesComponent';

const drawerWidth = 240;

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
  },
  appBarShift: {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(['width', 'margin'], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  menuButton: {
    marginLeft: 12,
    marginRight: 36,
  },
  hide: {
    display: 'none',
  },
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
    whiteSpace: 'nowrap',
  },
  drawerOpen: {
    width: drawerWidth,
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  },
  drawerClose: {
    transition: theme.transitions.create('width', {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.leavingScreen,
    }),
    overflowX: 'hidden',
    width: theme.spacing.unit * 7 + 1,
    [theme.breakpoints.up('sm')]: {
      width: theme.spacing.unit * 9 + 1,
    },
  },
  toolbar: {
    display: 'flex',
    alignItems: 'center',
    justifyContent: 'flex-end',
    padding: '0 8px',
    ...theme.mixins.toolbar,
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
});

class App extends Component {
  state = {
    open: false,
    selectedIndex: 1
  };

  handleListItemClick = (event, index) => {
    this.setState({ selectedIndex: index });
  };

  handleDrawerOpen = () => {
    this.setState({ open: true });
  };

  handleDrawerClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes, theme } = this.props;

    return (
      <Router>
        <div className={classes.root}>
          <CssBaseline />
          <AppBar
            position="fixed"
            className={classNames(classes.appBar, {
              [classes.appBarShift]: this.state.open,
            })}
          >
            <Toolbar disableGutters={!this.state.open}>
              <IconButton
                color="inherit"
                aria-label="Open drawer"
                onClick={this.handleDrawerOpen}
                className={classNames(classes.menuButton, {
                  [classes.hide]: this.state.open,
                })}
              >
                <MenuIcon />
              </IconButton>
              <Typography variant="h6" color="inherit" noWrap>
                Quickiz
              </Typography>
            </Toolbar>
          </AppBar>
          <Drawer
            variant="permanent"
            className={classNames(classes.drawer, {
              [classes.drawerOpen]: this.state.open,
              [classes.drawerClose]: !this.state.open,
            })}
            classes={{
              paper: classNames({
                [classes.drawerOpen]: this.state.open,
                [classes.drawerClose]: !this.state.open,
              }),
            }}
            open={this.state.open}
          >
            <div className={classes.toolbar}>
              <IconButton onClick={this.handleDrawerClose}>
                {theme.direction === 'rtl' ? <ChevronRightIcon /> : <ChevronLeftIcon />}
              </IconButton>
            </div>
            <List>
                <ListItem selected={this.state.selectedIndex === 0} onClick={event => this.handleListItemClick(event, 0)} button component={Link} to="/pools">
                  <ListItemIcon><StyleIcon/></ListItemIcon>
                  <ListItemText primary="Pozos" />
                </ListItem>
                <ListItem selected={this.state.selectedIndex === 1} onClick={event => this.handleListItemClick(event, 1)} button component={Link} to="/questions">
                  <ListItemIcon><HelpIcon/></ListItemIcon>
                  <ListItemText primary="Preguntas" />
                </ListItem>
                <ListItem selected={this.state.selectedIndex === 2} onClick={event => this.handleListItemClick(event, 2)} button component={Link} to="/quizzes">
                  <ListItemIcon><AssignmentIcon/></ListItemIcon>
                  <ListItemText primary="Quizzes" />
                </ListItem>
            </List>
            <Divider />
            <List>
                <ListItem selected={this.state.selectedIndex === 3} onClick={event => this.handleListItemClick(event, 3)} button component={Link} to="/my/quizzes">
                  <ListItemIcon><EditIcon /></ListItemIcon>
                  <ListItemText primary="Mis Quizzes" />
                </ListItem>
            </List>
          </Drawer>
          <main className={classes.content}>
            <div className={classes.toolbar} />
            <Switch>
              <Route exact path="/" component={HomeComponent} />

              <Route exact path="/pools" component={PoolIndexComponent} />
              <Route exact path="/pools/create" component={PoolCreateComponent} />
              <Route exact path="/pools/:id" component={PoolShowComponent} />
              <Route exact path="/pools/:id/edit" component={PoolEditComponent} />
              
              <Route exact path="/questions" component={QuestionIndexComponent} />
              <Route exact path="/questions/create" component={QuestionCreateComponent} />
              <Route exact path="/questions/:id" component={QuestionShowComponent} />
              <Route exact path="/questions/:id/edit" component={QuestionEditComponent} />

              <Route exact path="/quizzes" component={QuizIndexComponent} />
              <Route exact path="/quizzes/create" component={QuizCreateComponent} />
              <Route exact path="/quizzes/:id" component={QuizShowComponent} />
              <Route exact path="/quizzes/:id/edit" component={QuizEditComponent} />

              <Route exact path="/my/quizzes" component={MyQuizzesComponent} />
            </Switch>
          </main>
        </div>
      </Router>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(App);